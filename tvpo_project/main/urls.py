from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('send_numbers', views.get_numbers, name = 'send_numbers')
]