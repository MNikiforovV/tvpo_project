from django.test import TestCase
from . import views

def test_equation():
    assert views.equationroots(1, 3, -10) == {-5.0, 2.0}

def test_equation_zero_d():
    assert views.equationroots(1, 4, 4) == {-2.0}

def test_equation_below_zero_d():
    assert views.equationroots(1, 3, 5) == "No real roots"