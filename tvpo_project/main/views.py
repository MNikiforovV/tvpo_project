from http import HTTPStatus
from http.client import HTTPMessage, HTTPResponse
from django.shortcuts import render
import math

# adding commentarys on file

def index(request):
    return render(request, "index.html")

def get_numbers(request):
    if request.method == 'POST':
        try: 
            A = int(request.POST.get('A', 1))
        except:
            A = 1
        try:
            B = int(request.POST.get('B', 1))
        except:
            B = 1
        try:
            C = int(request.POST.get('C', 1))
        except:
            C = 1
    output = equationroots(A, B, C)
    return render(request, "index.html", {'output' : output})

#solving quadratic equation
def equationroots( x, y, z): 
    discri = y * y - 4 * x * z 
    sqrtval = math.sqrt(abs(discri)) 

    if discri > 0: 
        return ({(-y + sqrtval)/(2 * x), (-y - sqrtval)/(2 * x)})

    elif discri == 0: 
        return({(-y / (2 * x))})

    else:
        return "No real roots"
