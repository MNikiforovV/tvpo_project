import pytest

from django.urls import reverse

@pytest.mark.django_db
def test_view(client):
    url = reverse('index')
    response = client.get(url)
    assert response.status_code == 200

@pytest.mark.django_db
def test_view(client):
    url = reverse('send_numbers')
    response = client.post(url)
    assert response.status_code == 200